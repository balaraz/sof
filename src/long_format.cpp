/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#include "long_format.hpp"



std::string long_format(size_t bytes, const SizeStandard SIZE_STD) {
	const uint16_t UNIT_SIZE = static_cast<uint16_t>(SIZE_STD);
	std::string res = "";

	do {
		uint8_t exponent = 0;
		size_t x = bytes;

		while (x >= UNIT_SIZE && exponent < UNITS_NUM-1) {
			x /= UNIT_SIZE;
			++exponent;
		}
		res += std::format("{}{} ", x, UNITS[SIZE_STD/1024][exponent]);
		bytes -= x * std::pow<size_t>(UNIT_SIZE, exponent);
	} while (bytes > 0);

	return res;
}
