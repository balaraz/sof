/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#include "cmd_args.hpp"



CmdArgs::CmdArgs(TCLAP::CmdLine& cmd)
 : paths("paths", "Paths to show sizes", true, "path", cmd),
	use_si_standard("s", "SI", "Use SI standard", cmd, false),
	show_total_sum("t", "total-sum", "Show total sum of all path", cmd, false),
	quiet("q", "quiet", "Quiet mode", cmd, false),
	shows_only_filename("n", "only-filename", "Show only filename", cmd),
	shows_in_long_format("l", "long-format", "Show size in long format", cmd, false),
	shows_in_bytes("b", "bytes", "Show size in bytes", cmd, false),
	no_recursive("R", "no-recursive", "", cmd),
	skip_not_exist("e", "skip-not-exists", "Skip not exists paths", cmd),
	follow_symlinks("f", "follow-symlinks", "Follow symbolic links", cmd) {}
