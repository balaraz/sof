/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <tuple>

#include "cmd_args.hpp"
#include "cmd_output.hpp"
#include "short_format.hpp"
#include "long_format.hpp"


#define STRINGIZE_HELPER(x) #x
#define STRINGIZE(x) STRINGIZE_HELPER(x)

#ifndef SOF_VERSION
	#define SOF_VERSION unknown
#endif



namespace fs = std::filesystem;


std::tuple<char, size_t> path_size(fs::path, const bool, const bool);


int main(int argc, char* argv[]) {

	bool use_si_std,
		  shows_only_filename,
		  shows_in_long_format,
		  shows_in_bytes,
		  no_recursive,
		  sum_all,
		  quiet,
		  skip_not_exist,
		  follow_symlinks;

	std::vector<fs::path> paths;

	{ // Scope of CLI argument parsing
		CmdOutput cmd_output;
		TCLAP::CmdLine cmd("Find the size of files and directories.", ' ', STRINGIZE(SOF_VERSION));
		CmdArgs args(cmd);

		cmd.setOutput(&cmd_output);

		try {
			cmd.parse(argc, argv);

			// A manual check is used here, not the xorAdd function, since the second option requires one of them
			// to be specified, but in this case it is not required.
			if (args.shows_in_bytes.isSet() && args.shows_in_long_format.isSet())
				throw TCLAP::ArgException("\"-b\" and \"-l\" options are not compatible");

		}
		catch (TCLAP::ArgException& arg_ex) {
			try {
				cmd.getOutput()->failure(cmd, arg_ex);
			} catch (TCLAP::ExitException& exit_ex) {
				return exit_ex.getExitStatus();
			}
		}
		paths = args.paths.getValue();
		use_si_std = args.use_si_standard.getValue();
		sum_all = args.show_total_sum.getValue();
		quiet = args.quiet.getValue();
		shows_only_filename = args.shows_only_filename.getValue();
		shows_in_long_format = args.shows_in_long_format.getValue();
		shows_in_bytes = args.shows_in_bytes.getValue();
		no_recursive = args.no_recursive.getValue();
		skip_not_exist = args.skip_not_exist.getValue();
		follow_symlinks = args.follow_symlinks.getValue();
	}

	size_t total_size = 0;

	for (auto& path : paths) {
		if (!fs::exists(path)) {
			if (skip_not_exist)
				std::cerr << "w: " << path << " not exists" << std::endl;
			else {
				std::cerr << "e: " << path << " not exists" << std::endl;
				return 2;
			}
			continue;
		}

		try {
			auto [type_symbol, size] = path_size(path, no_recursive, follow_symlinks);

			if (sum_all)
				total_size += size;

			if (sum_all && quiet)
				continue;

			if (!quiet) {
				std::cout << type_symbol << ' ';

				if (shows_only_filename)
					std::cout << path.filename() << ' ';
				else
					std::cout << path.generic_string() << ' ';
			}

			if (shows_in_bytes)
				std::cout << size << 'B' << std::endl;
			else if (shows_in_long_format)
				std::cout << long_format(size, use_si_std ? SI : IEC) << std::endl;
			else
				std::cout << short_format(size, use_si_std ? SI : IEC) << std::endl;
		} catch (fs::filesystem_error& err) {
			std::cerr << err.what();
			return 3;
		}
	}

	if (sum_all) {
		if (!quiet)
			std::cout << "Total:\t";

		if (shows_in_bytes)
			std::cout << total_size << 'B' << std::endl;
		else if (shows_in_long_format)
			std::cout << long_format(total_size, use_si_std ? SI : IEC) << std::endl;
		else
			std::cout << short_format(total_size, use_si_std ? SI : IEC) << std::endl;
	}

	return 0;
}


std::tuple<char, size_t> path_size(fs::path path, const bool no_recursive, const bool follow_symlinks) {
	size_t size = 0;
	char type_symbol = ' ';

	auto dir_size = [&size](const fs::directory_entry& dentry){
		if (dentry.is_regular_file())
			size += dentry.file_size();
	};

	for (int8_t i=0; i<INT8_MAX; ++i) {
		// The loop is responsible for following symlinks

		if (fs::is_symlink(path)) {
			if (i <= 0)
				type_symbol = 'l';
			if (!follow_symlinks)
				break;
			fs::path unlinked_path = fs::read_symlink(path);
			if (path.has_parent_path() && unlinked_path.is_relative() && !unlinked_path.has_parent_path())
				path = path.parent_path() / unlinked_path;
			else
				path = unlinked_path;
		} else if (fs::is_regular_file(path)) {
			if (i <= 0)
				type_symbol = '-';
			size = fs::file_size(path);
			break;
		} else if (fs::is_directory(path)) {
			if (i <= 0)
				type_symbol = 'd';

			auto options = fs::directory_options::skip_permission_denied;
			if (follow_symlinks)
				options |= fs::directory_options::follow_directory_symlink;

			if (no_recursive)
				std::ranges::for_each(
					fs::directory_iterator(path, options),
					dir_size
				);
			else
				std::ranges::for_each(
					fs::recursive_directory_iterator(path, options),
					dir_size
				);
			break;
		} else {
			type_symbol = '?';
			break;
		}

		// In the previous block of code, the if statement and the std::filesystem::is_<type> functions
		// are used, not std::filesystem::file_status::type() and the switch statement, because in the second
		// case it is not possible to distinguish a symbolic link from a file which it points to.
	}

	return {type_symbol, size};
}
