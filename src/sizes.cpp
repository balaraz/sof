/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#include "sizes.hpp"



const char UNITS[2][UNITS_NUM][4] = {
	{"B", "kB", "mB", "gB", "tB", "pB", "eB"},
	{"B", "KiB", "MiB", "GiB", "TiB", "PiB", "ExB"}
};
