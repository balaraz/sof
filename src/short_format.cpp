/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#include "short_format.hpp"



std::string short_format(long double x, const SizeStandard SIZE_STD) {
	const auto UNIT_SIZE = static_cast<uint16_t>(SIZE_STD);
	uint8_t exponent = 0;

	while (x >= UNIT_SIZE && exponent < UNITS_NUM-1) {
		x /= UNIT_SIZE;
		++exponent;
	}

	return std::format("{:.3f}{}", x, UNITS[UNIT_SIZE/1024][exponent]);
}
