/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#include "cmd_output.hpp"



void CmdOutput::failure(TCLAP::CmdLineInterface& cli, TCLAP::ArgException& ae) {
	std::cerr << "e: " << ae.error() << std::endl;
	throw TCLAP::ExitException(1);
}


void CmdOutput::version(TCLAP::CmdLineInterface& cli) {
	std::cout << cli.getVersion() << std::endl;
}


void CmdOutput::usage(TCLAP::CmdLineInterface& cli) {
	std::cout << "Usage: " << cli.getProgramName() << ' ';

	const auto args = cli.getArgList();

	for (const TCLAP::Arg* arg : args) {
		std::cout << arg->shortID() << ' ';
	}

	std::cout << "\b\n\n" << cli.getMessage() << "\n\nOptions:\n";

	for (const auto arg : args) {
		std::cout << '\t' << arg->longID();

		std::cout << "\n\t\t" << std::setw(50) << std::left << arg->getDescription() << std::endl;
	}

	std::cout << "\nSee more info in man page. Run `man sof`.\n";
}
