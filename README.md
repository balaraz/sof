# SOF (Size OF)

A simple and fast program for calculating the size of a directory or file.


## License

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof If not, see <https://www.gnu.org/licenses/>.


## Install

- [From sources](./docs/sources.md)
- [Binaries](./docs/binaries.md)
- [Pakcges](./docs/packages.md)
