# Install binary files

If you don't want to build from source code, you can download binaries and integrate them into your system.

1. Download files

```sh
 $ VER="<version>" wget https://codeberg.org/balaraz/sof/releases/download/v$VER/sof-v$VER-linux-amd64
```

You need to replace `<version>' with the version you want to install.

```sh
 $ VER="3.0" wget https://codeberg.org/balaraz/sof/releases/download/v$VER/sof-v$VER-linux-amd64
```

2. Integrate files into your system

To integrate the files into your system, you need to move the files to the specified directories.

* User

```sh
 $ mv ./sof-v3.0-linux-amd64 -t ~/.local/bin
```

* System

```sh
 $ sudo mv ./sof-v3.0-linux-amd64 -t /usr/local/bin
```
