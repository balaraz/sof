# Install packge


## Arch Linux

### From releases page

1. Download package

You can download the package from the [releases page][releases].

```sh
 $ wget https://codeberg.org/balaraz/sof/releases/download/v3.0/sof-3.0-3-x86_64.pkg.tar.zst
```

> Only you may need to change the version in the link to the latest.
The latest version can be found on the [releases page][releases].

2. Install package

```sh
 $ sudo pacman -U ./sof-3.0-3-x86_64.pkg.tar.zst
```


### From AUR

You can install the program from the `AUR` repository in *Arch Linux*.
There are two packages for the program in AUR:

* [`sof`][sof]: It is built from the source code of the program.
* [`sof-bin`][sof-bin]: It uses ready binary files.


[releases]: https://codeberg.org/balaraz/sof/releases
[sof]: https://aur.archlinux.org/packages/sof
[sof-bin]: https://aur.archlinux.org/packages/sof-bin
