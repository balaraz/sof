# Build from sources and install


## 1. Download files

You can download the source code in two ways:

* Clone repository

```sh
 $ git clone https://codeberg.org/balaraz/sof.git --depth 1
```

* Download archive

```sh
 $ export SOF_VERSION=5.0 # You need to update this version to latest
 $ wget https://codeberg.org/balaraz/sof/archive/v$SOF_VERSION.tar.gz
 $ tar -xf v$SOF_VERSION.tar.gz
```


## 2. Requirements

Before starting the build, you need to install the following dependencies.

* [argumentum](https://github.com/mmahnic/argumentum/)

You can do this by following the instructions on their websites or try to find them in the package manager of your system distribution.


## 3. Build


```sh
 $ cmake -Bbuild -DCMAKE_BUILD_TYPE=Release
 $ cmake --build ./build
```


## 4. Install

* System

```sh
 $ sudo cmake --install ./build
```

* User

```sh
 $ cmake --install ./build --prefix $HOME/.local
```
