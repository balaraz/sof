
find_program(GZIP gzip)

if(GZIP)
	message(STATUS "GZip found: " ${GZIP})
else()
	message(STATUS "GZip not found")
endif()


function(make_gzips outvar input_files)
	if(NOT GZIP)
		message(FATAL_ERROR "GZip not found but required")
	endif()

	set(archive_files)

	foreach(file ${input_files})
		file(RELATIVE_PATH rel_file ${CMAKE_SOURCE_DIR} ${file})
		file(REAL_PATH ${rel_file} ofile BASE_DIRECTORY ${CMAKE_BINARY_DIR})

		set(ofile ${ofile}.gz)

		get_filename_component(dir ${ofile} DIRECTORY)
		file(MAKE_DIRECTORY ${dir})

		string(SHA1 target_name ${ofile})

		add_custom_target(
			${target_name} ALL ${GZIP} -c ${file} > ${ofile}
			COMMENT "Compressing ${rel_file} to GZip archive ${ofile}"
			BYPRODUCTS ${ofile}
			SOURCES ${file}
		)
		list(APPEND archive_files ${ofile})
	endforeach()

	set(${outvar} "${archive_files}" PARENT_SCOPE)
endfunction()
