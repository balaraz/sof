find_package(Git REQUIRED)



option(VERSION "Set default version of program if not detected git tags" "")


function(detect_version var_prefix)

	if(CMAKE_BUILD_TYPE MATCHES Release)
		set(DESCRIBE_OPTIONS --abbrev=0)
	else()
		set(DESCRIBE_OPTIONS --dirty --abbrev=8)
	endif()


	if(GIT_EXECUTABLE)
		execute_process(
			COMMAND ${GIT_EXECUTABLE} describe --tags --match "v[0-9]*.*" ${DESCRIBE_OPTIONS}
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
			OUTPUT_VARIABLE GIT_DESCRIBE_VERSION
			RESULT_VARIABLE GIT_DESCRIBE_ERROR_CODE
			OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET
		)
		message(STATUS "Git describe ended with code " ${GIT_DESCRIBE_ERROR_CODE})
		if(NOT GIT_DESCRIBE_ERROR_CODE)
			set(_VERSION ${GIT_DESCRIBE_VERSION})
		endif()
	endif()


	if(NOT DEFINED _VERSION)
		if(DEFINED VERSION AND VERSION)
			set(_VERSION ${VERSION})
		elseif(DEFINED ENV{${var_prefix}_VERSION} AND ENV{${var_prefix}_VERSION})
			set(_VERSION $ENV{${var_prefix}_VERSION})
		else()
			set(_VERSION "unknown")
		endif()
	endif()

	string(REPLACE "v" "" _VERSION ${_VERSION})

	message(STATUS "Version seted to " ${_VERSION})

	set(${var_prefix}_VERSION ${_VERSION} PARENT_SCOPE)

endfunction()
