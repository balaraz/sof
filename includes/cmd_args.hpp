/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <filesystem>

#include <tclap/CmdLine.h>


namespace fs = std::filesystem;


struct CmdArgs {
	TCLAP::UnlabeledMultiArg<fs::path> paths;
	TCLAP::SwitchArg
		use_si_standard,
		show_total_sum,
		quiet,
		shows_only_filename,
		shows_in_long_format,
		shows_in_bytes,
		no_recursive,
		skip_not_exist,
		follow_symlinks;

	CmdArgs(TCLAP::CmdLine&);
};
