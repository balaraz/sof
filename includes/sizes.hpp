/* Copyright (C) 2023, 2024 balaraz

This file is part of Sof.

Sof is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Sof is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Sof. If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstdint>



enum SizeStandard : unsigned short {
	SI  = 1000,
	IEC = 1024
};

inline constexpr uint8_t UNITS_NUM = 7;
extern const char UNITS[2][UNITS_NUM][4];
